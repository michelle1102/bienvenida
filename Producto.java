import javax.swing.JOptionPane;

public class Producto
{
	private String nombre;
	private int precio;
	private int existencias;
	
	public Producto (String nombre, int precio, int existencias)
	{
	setNombre(nombre);
	setPrecio(precio);
	setExistencias(existencias);
	}
	
	public void setNombre(String nombre)

	{
		this.nombre=nombre;
	}
	
	public String getNombre()
	{
		return nombre;
	}
	
	public void setPrecio(int precio)

	{
		this.precio=precio;
	}
	
	public int getPrecio()
	{
		return precio;
	}
	
	public void setExistencias(int existencias)

	{
		this.existencias=existencias;
	}
	
	public int getExistencias()
	{
		return existencias;
	}
	
	public String getInformacion()
	{
		return "El tipo de prenda es " +getNombre()+", con una precio de: " +getPrecio()+ "y un total de existencias es: " +getExistencias();
	}
	
	
}
	

